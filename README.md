# Dem downloader

## Requirements
The Snap gpt is required for the dem downloader.

Make sure Snap is installed on the pc your working on.

## Usage
Give the input raster for the required area of the dem -t.

Give the output location -o

Optional: give the variety of the dem 1sec or 3sec (default is 1sec)
