import argparse
import os
import download.download
import spatial.rasterize


parser = argparse.ArgumentParser(description="Get the corresponding DEM for an area")

parser.add_argument('--shp', '-s', required=True, help="Input area", type=str)
parser.add_argument('--dem', '-d', required=False, help="Preferred SRTM, 1sec or 3sec", default="1sec")

args = parser.parse_args()
input_shape = args.shp
dem_variety = args.dem
output_location = '\\\\10.0.0.4\\02_data\\00_base_data\\dem_data\\'

# transform the shape to a tiff
client_name = input_shape.split("\\")[5]
zone_number = input_shape.split("\\")[6]
year = input_shape.split("\\")[-2]
output_name = output_location + client_name + '_' + zone_number + '_dem.tif'

# make the output aoi tif
aoi_loc = 'D:/dem_temp/'

# check if D: exist
if not os.path.isdir("D:/"):
    aoi_loc = aoi_loc.replace('D:', 'C:')

if not os.path.isdir(aoi_loc):
    os.mkdir(aoi_loc)

aoi_tif = aoi_loc + client_name + '_' + zone_number + '_aoi.tif'

spatial.rasterize.rasterize(input_shape, aoi_tif)

if dem_variety == '1sec':
    xml_1sec = os.path.join(__file__, '..', 'download_dem_1sec.xml')
    download.download.download_dem(aoi_tif, xml_1sec, output_name)
else:
    xml_3sec = os.path.join(__file__, '..', 'download_dem_3sec_xml')
    download.download.download_dem(aoi_tif, xml_3sec, output_name)

os.remove(aoi_tif)
