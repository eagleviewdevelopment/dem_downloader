import gdal
import ogr
import osr


def rasterize(input_shape, output_image):

    # 1) opening the input_shape
    source_ds = ogr.Open(input_shape)
    source_layer = source_ds.GetLayer()

    # 2) Creating the destination raster data source
    pixelWidth = pixelHeight = 0.0001  # depending how fine you want your raster
    x_min, x_max, y_min, y_max = source_layer.GetExtent()
    cols = int((x_max - x_min) / pixelHeight)
    rows = int((y_max - y_min) / pixelWidth)

    target_ds = gdal.GetDriverByName('GTiff').Create(output_image, cols, rows, 1,
                                                     gdal.GDT_Float32)

    target_ds.SetGeoTransform((x_min, pixelWidth, 0, y_max, 0, -pixelHeight))

    # 3) Adding a spatial reference
    target_dsSRS = osr.SpatialReference()
    target_dsSRS.ImportFromEPSG(4326)
    target_ds.SetProjection(target_dsSRS.ExportToWkt())

    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(-9999)

    gdal.RasterizeLayer(target_ds, [1], source_layer, options=["ATTRIBUTE = UID"])

    target_ds = None
